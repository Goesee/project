window.onload = function(){
  const camera = document.getElementById("js--camera");
  const tutorialText = document.getElementById("js--tutorial-text");

  const pour = new Audio('sound/pour.wav');
  const drop = new Audio('sound/drop.wav');
  const stir = new Audio('sound/stir.mp3');

  const tutorialPlane = document.getElementById("js--tutorial-textplane");
  const tutorialPlaneTextMixer = document.getElementById("js--tutorial-textplane-mixer");
  const tutorialPlaneTextJigger = document.getElementById("js--tutorial-textplane-jigger");
  const tutorialPlaneTextLepel = document.getElementById("js--tutorial-textplane-lepel");
//pickup elementen
  const pickupsBottle = document.getElementsByClassName("js--pickup");
  const pickupsMixer = document.getElementsByClassName("js--pickup3");
  const pickupsLepel = document.getElementsByClassName("js--pickup4");
  const pickupsJigger = document.getElementsByClassName("js--pickup5");
  const pickupsCitroen = document.getElementsByClassName("js--pickup6");
  const pickupsTonic = document.getElementsByClassName("js--pickup7");
  const pickupsLime = document.getElementsByClassName("js--pickup8");
  const pickupsPeachSchnapps = document.getElementsByClassName("js--pickup9");
  const pickupsCranberryJuice = document.getElementsByClassName("js--pickup10");
  const pickupsGrapefruitJuice = document.getElementsByClassName("js--pickup11");
  const pickupsDryVermouth = document.getElementsByClassName("js--pickup12");
  const pickupsTequilla = document.getElementsByClassName("js--pickup13");
  const pickupsTripleSec = document.getElementsByClassName("js--pickup14");
  const pickupsLimeJuice = document.getElementsByClassName("js--pickup15");
  const pickupsLightRum = document.getElementsByClassName("js--pickup16");
  const pickupsSodaWater = document.getElementsByClassName("js--pickup17");
  const pickupsCoconutMilk = document.getElementsByClassName("js--pickup18");
  const pickupsVodka = document.getElementsByClassName("js--pickup24");

  const pickupsOlive = document.getElementsByClassName("js--pickup19");
  const pickupsSalt = document.getElementsByClassName("js--pickup20");
  const pickupsSugar = document.getElementsByClassName("js--pickup21");
  const pickupsMint = document.getElementsByClassName("js--pickup22");
  const pickupsPineapple = document.getElementsByClassName("js--pickup23");
//put down discs
  const placeholders = document.getElementsByClassName("js--glassplaceholder");
  const tonicPlaceholder = document.getElementsByClassName("js--tonicPlaceholder");
  const ginPlaceholder = document.getElementsByClassName("js--ginPlaceholder");
  const lepelPlaceholder = document.getElementsByClassName("js--lepelPlaceholder");
  const jiggerPlaceholder = document.getElementsByClassName("js--jiggerPlaceholder");
  const mixerPlaceholder = document.getElementsByClassName("js--mixerPlaceholder");
  const lemonPlaceholder = document.getElementsByClassName("js--lemonPlaceholder");
  const limePlaceholder = document.getElementsByClassName("js--limePlaceholder");
  const grapefruitJuicePlaceholder = document.getElementsByClassName("js--grapefruitJuicePlaceholder");
  const dryVermouthPlaceholder = document.getElementsByClassName("js--dryVermouthPlaceholder");
  const tequillaPlaceholder = document.getElementsByClassName("js--tequillaPlaceholder");
  const tripleSecPlaceholder = document.getElementsByClassName("js--tripleSecPlaceholder");
  const limeJuicePlaceholder = document.getElementsByClassName("js--limeJuicePlaceholder");
  const lightRumPlaceholder = document.getElementsByClassName("js--lightRumPlaceholder");
  const sodaWaterPlaceholder = document.getElementsByClassName("js--sodaWaterPlaceholder");
  const coconutMilkPlaceholder = document.getElementsByClassName("js--coconutMilkPlaceholder");
  const cranberryJuicePlaceholder = document.getElementsByClassName("js--cranberryJuicePlaceholder");
  const peachSchnappsPlaceholder = document.getElementsByClassName("js--peachSchnappsPlaceholder");
  const vodkaPlaceholder = document.getElementsByClassName("js--vodkaPlaceholder");

  const olivePlaceholder = document.getElementsByClassName("js--olivePlaceholder");
  const saltPlaceholder = document.getElementsByClassName("js--saltPlaceholder");
  const sugarPlaceholder = document.getElementsByClassName("js--sugarPlaceholder");
  const mintPlaceholder = document.getElementsByClassName("js--mintPlaceholder");
  const pineapplePlaceholder = document.getElementsByClassName("js--pineapplePlaceholder");

  const tutorialCheckList = document.getElementsByClassName("js--tutorial-checkList");
  const scene = document.getElementById("js--scene");
//drank menu
  const drinkPictures = document.getElementsByClassName("js--drinkpicture");
  const drinkNames = document.getElementsByClassName("js--drinkname");
  const menuBoard = document.getElementById("js--menuboard");
  const BASE_URL = "https://www.thecocktaildb.com/api/json/v1/1/search.php?s=";

  const serveButton = document.getElementsByClassName("js--serveButton");
  const skipButton = document.getElementsByClassName("js--skipButton");
  const resetButton = document.getElementsByClassName("js--resetButton");

  const resetableObjects = document.getElementsByClassName("js--reset");
// hand hold
  let hold = null;
  let plate = "empty";
//tutorial object booleans
  let tutorial_Mixer = false;
  let tutorial_Jigger = false;
  let tutorial_Lepel = false;

  let jiggerContents ="";

  let drinkCount = 0;


  let ginAdded = false;
  let tonicAdded = false;
  let lemonAdded = false;
  let vodkaAdded = false;
  let limeAdded = false;
  let peachSchnappsAdded = false;
  let cranberryJuiceAdded = false;
  let grapefruitJuiceAdded = false;
  let dryVermouthAdded = false;
  let oliveAdded = false;
  let tequilaAdded = false;
  let tripleSecAdded = false;
  let limeJuiceAdded = false;
  let saltAdded = false;
  let lightRumAdded = false;
  let sugarAdded = false;
  let mintAdded = false;
  let sodaWaterAdded = false;
  let coconutMilkAdded = false;
  let pineappleAdded = false;
  let mixerContents = [];

// set modes
  let objective = null;
  let tutorial = true;
  let drinkToBeMade = null;

//check if items used
  let drinkStirred = false;
  let mixerUsed = false;
  let jiggerUsed = false;

//roep de begin functies aan
  openRecipe();

  addListenersMixer();
  addListenersJigger();
  addListenersLepel();
  makeGlass();

// tutorial checklist
  checkTutorialMixer = () =>{
    if(tutorial_Mixer == true){
      tutorialCheckList[1].setAttribute("color", "green");
    }
  }

  checkTutorialJigger = () =>{
    if(tutorial_Jigger == true){
      tutorialCheckList[2].setAttribute("color", "green");
    }
  }

  checkTutorialLepel = () =>{
    if(tutorial_Lepel == true){
      tutorialCheckList[3].setAttribute("color", "green");
    }
  }

  checkTutorial = () =>{
    if(tutorial_Mixer == true && tutorial_Jigger == true && tutorial_Lepel == true){
       tutorial = false;
         resetObjects();
         setDrinkToBeMade();
         hold = null;
         addListenersMixer();
         addListenersJigger();
         addListenersLepel2();
         addListenersLemon();
         addListenersLime();
         addListenersGrapefruitJuice();
         addListenersDryVermouth();
         addListenersTequilla();
         addListenersTripleSec();
         addListenersLimeJuice();
         addListenersLightRum();
         addListenersSodaWater();
         addListenersCoconutMilk();
         addListenersTonic();
         addListenersGin();
         addListenersCranberryJuice();
         addListenersPeachSchnapps();
         addListenersOlive();
         addListenersSalt();
         addListenersSugar();
         addListenersMint();
         addListenersPineapple();
         addListenersVodka();
         removeElementsByClassName("js--skipButton");
         removeElementsByClassName("js--tutorial-text");
         let check = document.getElementById("js--hold");
         if(check){
           document.getElementById("js--hold").remove();
         }
     } else {
     }
  }

  function makeGlass(){
    let glass = document.createElement("a-entity");
    glass.setAttribute("class", "js--glassOnPlate clickable");
    glass.setAttribute("gltf-model", "#glass1-glb");
    glass.setAttribute("scale", "0.08 0.08 0.08");
    glass.setAttribute("position", {x: -0.3, y: 1.5, z: -2.2});
    scene.appendChild(glass);
    addGlasslisteners();
  }

  function resetObjects(){
    removeElementsByClassName("js--reset");
    removeElementsByClassName("js--tutorial-checkList");
// opnieuw object aanmaken als je hem terug zet
    let glass = document.createElement("a-entity");
    glass.setAttribute("class", "js--glassOnPlate clickable");
    glass.setAttribute("gltf-model", "#glass1-glb");
    glass.setAttribute("scale", "0.08 0.08 0.08");
    glass.setAttribute("position", {x: -0.3, y: 1.5, z: -2.2});
    scene.appendChild(glass);
    addGlasslisteners();

    let jigger = document.createElement("a-entity");
    jigger.setAttribute("class", "js--pickup5 clickable js--reset");
    jigger.setAttribute("gltf-model", "#jigger-glb");
    jigger.setAttribute("scale", "0.08 0.08 0.08");
    jigger.setAttribute("position", {x: -1, y: 1.3, z: -1.5});
    scene.appendChild(jigger);

    let mixer = document.createElement("a-entity");
    mixer.setAttribute("class", "js--pickup3 clickable js--reset");
    mixer.setAttribute("gltf-model", "#mixer-glb");
    mixer.setAttribute("scale", "0.08 0.08 0.08");
    mixer.setAttribute("position", {x: -0.7, y: 1.33, z: -1.5});
    scene.appendChild(mixer);

    let lepel = document.createElement("a-entity");
    lepel.setAttribute("class", "js--pickup4 clickable js--reset");
    lepel.setAttribute("gltf-model", "#lepel-glb");
    lepel.setAttribute("scale", "0.08 0.08 0.08");
    lepel.setAttribute("position", {x: -0.2, y: 1.33, z: -1.5});
    scene.appendChild(lepel);

  }
// pickup garnish
  function addListenersLime(){
  if(tutorial == false){
    for(let i = 0; i < pickupsLime.length; i++){
        pickupsLime[i].addEventListener('click', function(evt){
          if(hold == null){
            hold="lime";
            camera.innerHTML += '<a-entity id="js--hold" gltf-model="#lime-glb" value="lime" position ="0.5 0 -1" scale="0.15 0.15 0.15"></a-entity>';
            this.remove();
          }
        });
      }
    }}

  function addListenersLemon(){
  if(tutorial == false){
    for(let i = 0; i < pickupsCitroen.length; i++){
        pickupsCitroen[i].addEventListener('click', function(evt){
          if(hold == null){
            hold="lemon";
            camera.innerHTML += '<a-entity id="js--hold" value="lemon" gltf-model="#citroen-glb" value="lemon" position ="0.5 0 -1" scale="0.15 0.15 0.15"></a-entity>';
            this.remove();
          }
        });
      }
  }}
  function addListenersSalt(){
    if(tutorial == false){
      for(let i = 0; i < pickupsSalt.length; i++){
        pickupsSalt[i].addEventListener('click', function(evt){
        if(hold == null){
          hold="salt";
          camera.innerHTML += '<a-entity id="js--hold" value="salt" gltf-model="#zout-glb" position ="0.5 0 -1" scale="0.15 0.15 0.15"></a-entity>';
          this.remove();
        }
      });
    }
  }}

  function addListenersOlive(){
    if(tutorial == false){
      for(let i = 0; i < pickupsOlive.length; i++){
        pickupsOlive[i].addEventListener('click', function(evt){
        if(hold == null){
          hold="olive";
          camera.innerHTML += '<a-entity id="js--hold" value="olive" gltf-model="#olive-glb" position ="0.5 0 -1" scale="0.15 0.15 0.15"></a-entity>';
          this.remove();
        }
      });
    }
  }}

  function addListenersSugar(){
    if(tutorial == false){
      for(let i = 0; i < pickupsSugar.length; i++){
        pickupsSugar[i].addEventListener('click', function(evt){
        if(hold == null){
          hold="sugar";
          camera.innerHTML += '<a-entity id="js--hold" value="sugar" gltf-model="#suiker-glb" position ="0.5 0 -1" scale="0.15 0.15 0.15"></a-entity>';
          this.remove();
        }
      });
    }
  }}

  function addListenersMint(){
    if(tutorial == false){
      for(let i = 0; i < pickupsMint.length; i++){
        pickupsMint[i].addEventListener('click', function(evt){
        if(hold == null){
          hold="mint";
          camera.innerHTML += '<a-box id="js--hold" value="mint" src="./textures/munt.jpg" position ="0.5 0 -1" scale="0.15 0.15 0.15"></a-entity>';
          this.remove();
        }
      });
    }
  }}

  function addListenersPineapple(){
    if(tutorial == false){
      for(let i = 0; i < pickupsPineapple.length; i++){
        pickupsPineapple[i].addEventListener('click', function(evt){
        if(hold == null){
          hold="pineapple";
          camera.innerHTML += '<a-entity id="js--hold" value="pineapple" gltf-model="#ananas-glb" position ="0.5 0 -1" scale="0.15 0.15 0.15"></a-entity>';
          this.remove();
        }
      });
    }
  }}
//pickups dranken
  function addListenersGin(){
    if(tutorial == false){
      for(let i = 0; i < pickupsBottle.length; i++){
        pickupsBottle[i].addEventListener('click', function(evt){
        if(hold == null){
          hold="gin";
          camera.innerHTML += '<a-entity id="js--hold" value="gin" gltf-model="#bottle-glb" position ="0.5 0 -1" scale="0.15 0.15 0.15"></a-entity>';
          this.remove();
        }
      });
    }
  }}

  function addListenersTonic(){
      if(tutorial == false){
      for(let i = 0; i < pickupsTonic.length; i++){
          pickupsTonic[i].addEventListener('click', function(evt){
            if(hold == null){
              hold="tonic";
              camera.innerHTML += '<a-entity id="js--hold" value="tonic" gltf-model="#bottle-glb" position ="0.5 0 -1" scale="0.15 0.15 0.15"></a-entity>';
              this.remove();
            }
          });
        }
      }}

  function addListenersPeachSchnapps(){
        if(tutorial == false){
        for(let i = 0; i < pickupsPeachSchnapps.length; i++){
            pickupsPeachSchnapps[i].addEventListener('click', function(evt){
              if(hold == null){
                hold="peachSchnapps";
                camera.innerHTML += '<a-entity id="js--hold" value="peachSchnapps" gltf-model="#bottle-glb" position ="0.5 0 -1" scale="0.15 0.15 0.15"></a-entity>';
                this.remove();
              }
            });
          }
        }}

  function addListenersCranberryJuice(){
        if(tutorial == false){
        for(let i = 0; i < pickupsCranberryJuice.length; i++){
            pickupsCranberryJuice[i].addEventListener('click', function(evt){
              if(hold == null){
                hold="cranberryJuice";
                camera.innerHTML += '<a-entity id="js--hold" value="cranberryJuice" gltf-model="#bottle-glb" position ="0.5 0 -1" scale="0.15 0.15 0.15"></a-entity>';
                this.remove();
              }
            });
          }
        }}

  function addListenersGrapefruitJuice(){
        if(tutorial == false){
        for(let i = 0; i < pickupsGrapefruitJuice.length; i++){
            pickupsGrapefruitJuice[i].addEventListener('click', function(evt){
              if(hold == null){
                hold="grapeFruitJuice";
                camera.innerHTML += '<a-entity id="js--hold" value="grapeFruitJuice" gltf-model="#bottle-glb" position ="0.5 0 -1" scale="0.15 0.15 0.15"></a-entity>';
                this.remove();
              }
            });
          }
        }}

  function addListenersDryVermouth(){
          if(tutorial == false){
          for(let i = 0; i < pickupsDryVermouth.length; i++){
              pickupsDryVermouth[i].addEventListener('click', function(evt){
                if(hold == null){
                  hold="dryVermouth";
                  camera.innerHTML += '<a-entity id="js--hold" value="dryVermouth" gltf-model="#bottle-glb" position ="0.5 0 -1" scale="0.15 0.15 0.15"></a-entity>';
                  this.remove();
                }
              });
            }
          }}

  function addListenersTequilla(){
            if(tutorial == false){
            for(let i = 0; i < pickupsTequilla.length; i++){
                pickupsTequilla[i].addEventListener('click', function(evt){
                  if(hold == null){
                    hold="tequilla";
                    camera.innerHTML += '<a-entity id="js--hold" value="tequilla" gltf-model="#bottle-glb" position ="0.5 0 -1" scale="0.15 0.15 0.15"></a-entity>';
                    this.remove();
                  }
                });
              }
            }}

  function addListenersTripleSec(){
              if(tutorial == false){
              for(let i = 0; i < pickupsTripleSec.length; i++){
                  pickupsTripleSec[i].addEventListener('click', function(evt){
                    if(hold == null){
                      hold="tripleSec";
                      camera.innerHTML += '<a-entity id="js--hold" value="tripleSec" gltf-model="#bottle-glb" position ="0.5 0 -1" scale="0.15 0.15 0.15"></a-entity>';
                      this.remove();
                    }
                  });
                }
              }}

  function addListenersLimeJuice(){
                if(tutorial == false){
                for(let i = 0; i < pickupsLimeJuice.length; i++){
                    pickupsLimeJuice[i].addEventListener('click', function(evt){
                      if(hold == null){
                        hold="limeJuice";
                        camera.innerHTML += '<a-entity id="js--hold" value="limeJuice" gltf-model="#bottle-glb" position ="0.5 0 -1" scale="0.15 0.15 0.15"></a-entity>';
                        this.remove();
                      }
                    });
                  }
                }}

  function addListenersLightRum(){
                  if(tutorial == false){
                  for(let i = 0; i < pickupsLightRum.length; i++){
                      pickupsLightRum[i].addEventListener('click', function(evt){
                        if(hold == null){
                          hold="lightRum";
                          camera.innerHTML += '<a-entity id="js--hold" value="lightRum" gltf-model="#bottle-glb" position ="0.5 0 -1" scale="0.15 0.15 0.15"></a-entity>';
                          this.remove();
                        }
                      });
                    }
                  }}

  function addListenersSodaWater(){
                    if(tutorial == false){
                    for(let i = 0; i < pickupsSodaWater.length; i++){
                        pickupsSodaWater[i].addEventListener('click', function(evt){
                          if(hold == null){
                            hold="sodaWater";
                            camera.innerHTML += '<a-entity id="js--hold" value="sodaWater" gltf-model="#bottle-glb" position ="0.5 0 -1" scale="0.15 0.15 0.15"></a-entity>';
                            this.remove();
                          }
                        });
                      }
                    }}
  function addListenersVodka(){
    if(tutorial == false){
      for(let i = 0; i < pickupsVodka.length; i++){
        pickupsVodka[i].addEventListener('click', function(evt){
          if(hold == null){
            hold="vodka";
            camera.innerHTML += '<a-entity id="js--hold" value="vodka" gltf-model="#bottle-glb" position ="0.5 0 -1" scale="0.15 0.15 0.15"></a-entity>';
            this.remove();
          }
        });
      }
    }}

  function addListenersCoconutMilk(){
    if(tutorial == false){
      for(let i = 0; i < pickupsCoconutMilk.length; i++){
        pickupsCoconutMilk[i].addEventListener('click', function(evt){
        if(hold == null){
          hold="coconutMilk";
          camera.innerHTML += '<a-entity id="js--hold" value="coconutMilk" gltf-model="#bottle-glb" position ="0.5 0 -1" scale="0.15 0.15 0.15"></a-entity>';
          this.remove();
        }
      });
    }
  }}
//pickups mixset
  function addListenersMixer(){
    if(tutorial){
      for(let i = 0; i < pickupsMixer.length; i++){
          pickupsMixer[i].addEventListener('click', function(evt){
            if(hold == null){
              hold="mixer";
              tutorial_Mixer = true;
              camera.innerHTML += '<a-entity id="js--hold" gltf-model="#mixer-glb" position ="-.3 -.2 -.5" scale="0.05 0.05 0.05"></a-entity>';
              tutorialPlaneTextMixer.setAttribute("visible", "true");
              tutorialPlane.setAttribute("visible", "true");
              this.remove();
              checkTutorialMixer();
            }
          });
        }
      }else if(!tutorial){
        for(let i = 0; i < pickupsMixer.length; i++){
            pickupsMixer[i].addEventListener('click', function(evt){
              if(hold == null){
                hold="mixer";
                camera.innerHTML += '<a-entity id="js--hold" gltf-model="#mixer-glb" position ="0.5 0 -1" scale="0.15 0.15 0.15"></a-entity>';
                this.remove();
              }else if(hold == "jigger"){
                pour.play();
                mixerContents.push(jiggerContents);
                jiggerUsed = true;
                jiggerContents = "";
              }else if(hold != null && hold != "jigger" && hold != "mixer" && hold != "lepel" && hold != "lime" && hold != "lemon" && hold != "olive" && hold != "salt" && hold != "sugar" && hold != "mint" && hold != "pineapple"){
                pour.play();
                mixerContents.push(document.getElementById("js--hold").getAttribute("value"));
              }
              for(let i = 0; i < mixerContents.length; i++){
                console.log(mixerContents[i]);
              }
            });
          }
        }
      }

  function addListenersLepel(){
    if(tutorial){
      for(let i = 0; i < pickupsLepel.length; i++){
          pickupsLepel[i].addEventListener('click', function(evt){
            if(hold == null){
              hold="lepel";
              tutorial_Lepel = true;
              camera.innerHTML += '<a-entity id="js--hold" gltf-model="#lepel-glb" position ="0 -.1 -.5" scale="0.05 0.05 0.05"></a-entity>';
              tutorialPlane.setAttribute("visible", "true");
              tutorialPlaneTextLepel.setAttribute("visible", "true");
              this.remove();
              checkTutorialLepel();
            }
          });
        }
      }}

  function addListenersLepel2(){
    if(tutorial == false){
      for(let i = 0; i < pickupsLepel.length; i++){
          pickupsLepel[i].addEventListener('click', function(evt){
            if(hold == null){
              hold="lepel";
              camera.innerHTML += '<a-entity id="js--hold" gltf-model="#lepel-glb" position ="0.5 0 -1" scale="0.15 0.15 0.15" rotation="0 -90 -60"></a-entity>';
              this.remove();
            }
          });
        }
      }
  }

  function addListenersJigger(){
    if(tutorial){
      for(let i = 0; i < pickupsJigger.length; i++){
          pickupsJigger[i].addEventListener('click', function(evt){
            if(hold == null){
              hold="jigger";
              tutorial_Jigger = true;
              camera.innerHTML += '<a-entity id="js--hold" gltf-model="#jigger-glb" position ="-.3 0 -0.5" scale="0.05 0.05 0.05" value=""></a-entity>';
              tutorialPlane.setAttribute("visible", "true");
              tutorialPlaneTextJigger.setAttribute("visible", "true");
              this.remove();
              checkTutorialJigger();
            }
          });
        }
      } else if(!tutorial) {
    for(let i = 0; i < pickupsJigger.length; i++){
        pickupsJigger[i].addEventListener('click', function(evt){
          if(hold == null){
            hold="jigger";
            camera.innerHTML += '<a-entity id="js--hold" gltf-model="#jigger-glb" position ="0.5 0 -1" scale="0.15 0.15 0.15"></a-entity>';
            this.remove();
          }else if(hold != null && hold != "jigger" && hold != "mixer" && hold != "lepel" && hold != "lime" && hold != "lemon" && hold != "olive" && hold != "salt" && hold != "sugar" && hold != "mint" && hold != "pineapple"){
            pour.play();
            jiggerContents = document.getElementById("js--hold").getAttribute("value");
          }
        });
      }
    }}
//put down garnish
  for(let i = 0; i < lemonPlaceholder.length; i++){
    lemonPlaceholder[i].addEventListener('click', function(evt){
      if(hold=="lemon"){
        hold = null;
        let glass = document.createElement("a-entity");
        glass.setAttribute("class", "js--pickup6 clickable");
        glass.setAttribute("gltf-model", "#citroen-glb");
        glass.setAttribute("scale", "0.15 0.15 0.15");
        glass.setAttribute("position", {x: this.getAttribute("position").x, y: 1.4, z: this.getAttribute("position").z});
        scene.appendChild(glass);
        glass.innerHTML += '<a-text position="-0.6 0 0" align="center" scale="1.3 1.3 1.3" rotation="0 -90 0" value="Lemon">';
        addListenersLemon();
        document.getElementById("js--hold").remove();
      }
    });
  }

  for(let i = 0; i < limePlaceholder.length; i++){
    limePlaceholder[i].addEventListener('click', function(evt){
      if(hold=="lime"){
        hold = null;
        let glass = document.createElement("a-entity");
        glass.setAttribute("class", "js--pickup8 clickable");
        glass.setAttribute("gltf-model", "#lime-glb");
        glass.setAttribute("scale", "0.15 0.15 0.15");
        glass.setAttribute("position", {x: this.getAttribute("position").x, y: 1.4, z: this.getAttribute("position").z});
        scene.appendChild(glass);
        glass.innerHTML += '<a-text position="-0.6 0 0" align="center" scale="1.3 1.3 1.3" rotation="0 -90 0" value="Lime">';
        addListenersLime();
        document.getElementById("js--hold").remove();
      }
    });
  }

  for(let i = 0; i < ginPlaceholder.length; i++){
    ginPlaceholder[i].addEventListener('click', function(evt){
      if(hold=="gin"){
        hold = null;
        let glass = document.createElement("a-entity");
        glass.setAttribute("class", "js--pickup clickable");
        glass.setAttribute("gltf-model", "#bottle-glb");
        glass.setAttribute("scale", "0.15 0.15 0.15");
        glass.setAttribute("rotation", "0 90 0");
        glass.setAttribute("position", {x: this.getAttribute("position").x, y: 1.6, z: this.getAttribute("position").z});
        scene.appendChild(glass);
        glass.innerHTML += '<a-text position="-0.6 0 0" align="center" scale="1.3 1.3 1.3" rotation="0 -90 0" value="Gin">';
        addListenersGin();
        document.getElementById("js--hold").remove();
      }
    });
  }

  for(let i = 0; i < olivePlaceholder.length; i++){
    olivePlaceholder[i].addEventListener('click', function(evt){
      if(hold=="olive"){
        hold = null;
        let glass = document.createElement("a-entity");
        glass.setAttribute("class", "js--pickup19 clickable");
        glass.setAttribute("gltf-model", "#olive-glb");
        glass.setAttribute("scale", "0.05 0.05 0.05");
        glass.setAttribute("position", {x: this.getAttribute("position").x, y: 1.45, z: this.getAttribute("position").z});
        scene.appendChild(glass);
        glass.innerHTML += '<a-text position="-1 0 0" align="center" scale="4 4 4" rotation="0 -90 0" value="Olive">';
        addListenersOlive();
        document.getElementById("js--hold").remove();
      }
    });
  }

  for(let i = 0; i < saltPlaceholder.length; i++){
    saltPlaceholder[i].addEventListener('click', function(evt){
      if(hold=="salt"){
        hold = null;
        let glass = document.createElement("a-entity");
        glass.setAttribute("class", "js--pickup20 clickable");
        glass.setAttribute("gltf-model", "#zout-glb");
        glass.setAttribute("scale", "0.05 0.05 0.05");
        glass.setAttribute("position", {x: this.getAttribute("position").x, y: 1.45, z: this.getAttribute("position").z});
        scene.appendChild(glass);
        glass.innerHTML += '<a-text position="-2 0 0" align="center" scale="4 4 4" rotation="0 -90 0" value="Salt">';
        addListenersSalt();
        document.getElementById("js--hold").remove();
      }
    });
  }

  for(let i = 0; i < sugarPlaceholder.length; i++){
    sugarPlaceholder[i].addEventListener('click', function(evt){
      if(hold=="sugar"){
        hold = null;
        let glass = document.createElement("a-entity");
        glass.setAttribute("class", "js--pickup21 clickable");
        glass.setAttribute("gltf-model", "#suiker-glb");
        glass.setAttribute("scale", "0.05 0.05 0.05");
        glass.setAttribute("position", {x: this.getAttribute("position").x, y: 1.45, z: this.getAttribute("position").z});
        scene.appendChild(glass);
        glass.innerHTML += '<a-text position="-2 0 0" align="center" scale="4 4 4" rotation="0 -90 0" value="Sugar">';
        addListenersSugar();
        document.getElementById("js--hold").remove();
      }
    });
  }

  for(let i = 0; i < mintPlaceholder.length; i++){
    mintPlaceholder[i].addEventListener('click', function(evt){
      if(hold=="mint"){
        hold = null;
        let glass = document.createElement("a-box");
        glass.setAttribute("class", "js--pickup22 clickable");
        glass.setAttribute("src", "./textures/munt.jpg");
        glass.setAttribute("scale", "0.1 0.1 0.1");
        glass.setAttribute("position", {x: this.getAttribute("position").x, y: 1.45, z: this.getAttribute("position").z});
        scene.appendChild(glass);
        glass.innerHTML += '<a-text position="-1 0 0" align="center" scale="4 4 4" rotation="0 -90 0" value="Mint">';
        addListenersMint();
        document.getElementById("js--hold").remove();
      }
    });
  }

  for(let i = 0; i < pineapplePlaceholder.length; i++){
    pineapplePlaceholder[i].addEventListener('click', function(evt){
      if(hold=="pineapple"){
        hold = null;
        let glass = document.createElement("a-entity");
        glass.setAttribute("class", "js--pickup23 clickable");
        glass.setAttribute("gltf-model", "#ananas-glb");
        glass.setAttribute("scale", "0.05 0.05 0.05");
        glass.setAttribute("position", {x: this.getAttribute("position").x, y: 1.45, z: this.getAttribute("position").z});
        scene.appendChild(glass);
        glass.innerHTML += '<a-text position="-1 0 0" align="center" scale="4 4 4" rotation="0 -90 0" value="Pineapple">';
        addListenersPineapple();
        document.getElementById("js--hold").remove();
      }
    });
  }
  //put down drank
  for(let i = 0; i < ginPlaceholder.length; i++){
    ginPlaceholder[i].addEventListener('click', function(evt){
      if(hold=="gin"){
        hold = null;
        let glass = document.createElement("a-entity");
        glass.setAttribute("class", "js--pickup clickable");
        glass.setAttribute("gltf-model", "#bottle-glb");
        glass.setAttribute("scale", "0.15 0.15 0.15");
        glass.setAttribute("position", {x: this.getAttribute("position").x, y: 1.6, z: this.getAttribute("position").z});
        scene.appendChild(glass);
        glass.innerHTML += '<a-text position="-0.6 0 0" align="center" scale="1.3 1.3 1.3" rotation="0 -90 0" value="Gin">';
        addListenersGin();
        document.getElementById("js--hold").remove();
      }
    });
  }

  for(let i = 0; i < cranberryJuicePlaceholder.length; i++){
    cranberryJuicePlaceholder[i].addEventListener('click', function(evt){
      if(hold=="cranberryJuice"){
        hold = null;
        let glass = document.createElement("a-entity");
        glass.setAttribute("class", "js--pickup10 clickable");
        glass.setAttribute("gltf-model", "#bottle-glb");
        glass.setAttribute("scale", "0.15 0.15 0.15");
        glass.setAttribute("rotation", "0 90 0");
        glass.setAttribute("position", {x: this.getAttribute("position").x, y: 1.6, z: this.getAttribute("position").z});
        scene.appendChild(glass);
        glass.innerHTML += '<a-text position="-0.6 0 0" align="center" scale="1.3 1.3 1.3" rotation="0 -90 0" value="Cranberry Juice">';
        addListenersCranberryJuice();
        document.getElementById("js--hold").remove();
      }
    });
  }

  for(let i = 0; i < peachSchnappsPlaceholder.length; i++){
    peachSchnappsPlaceholder[i].addEventListener('click', function(evt){
      if(hold=="peachSchnapps"){
        hold = null;
        let glass = document.createElement("a-entity");
        glass.setAttribute("class", "js--pickup9 clickable");
        glass.setAttribute("gltf-model", "#bottle-glb");
        glass.setAttribute("scale", "0.15 0.15 0.15");
        glass.setAttribute("rotation", "0 90 0");
        glass.setAttribute("position", {x: this.getAttribute("position").x, y: 1.6, z: this.getAttribute("position").z});
        scene.appendChild(glass);
        glass.innerHTML += '<a-text position="-0.6 0 0" align="center" scale="1.3 1.3 1.3" rotation="0 -90 0" value="Peach Schnapps">';
        addListenersPeachSchnapps();
        document.getElementById("js--hold").remove();
      }
    });
  }

  for(let i = 0; i < tonicPlaceholder.length; i++){
    tonicPlaceholder[i].addEventListener('click', function(evt){
      if(hold=="tonic"){
        hold = null;
        let glass = document.createElement("a-entity");
        glass.setAttribute("class", "js--pickup7 clickable");
        glass.setAttribute("gltf-model", "#bottle-glb");
        glass.setAttribute("scale", "0.15 0.15 0.15");
        glass.setAttribute("rotation", "0 90 0");
        glass.setAttribute("position", {x: this.getAttribute("position").x, y: 1.6, z: this.getAttribute("position").z});
        scene.appendChild(glass);
        glass.innerHTML += '<a-text position="-0.6 0 0" align="center" scale="1.3 1.3 1.3" rotation="0 -90 0" value="Tonic">';
        addListenersTonic();
        document.getElementById("js--hold").remove();
      }
    });
  }

  for(let i = 0; i < grapefruitJuicePlaceholder.length; i++){
    grapefruitJuicePlaceholder[i].addEventListener('click', function(evt){
      if(hold=="grapeFruitJuice"){
        hold = null;
        let glass = document.createElement("a-entity");
        glass.setAttribute("class", "js--pickup11 clickable");
        glass.setAttribute("gltf-model", "#bottle-glb");
        glass.setAttribute("scale", "0.15 0.15 0.15");
        glass.setAttribute("rotation", "0 90 0");
        glass.setAttribute("position", {x: this.getAttribute("position").x, y: 1.6, z: this.getAttribute("position").z});
        scene.appendChild(glass);
        glass.innerHTML += '<a-text position="-0.6 0 0" align="center" scale="1.3 1.3 1.3" rotation="0 -90 0" value="Grapefruit Juice">';
        addListenersGrapefruitJuice();
        document.getElementById("js--hold").remove();
      }
    });
  }

  for(let i = 0; i < dryVermouthPlaceholder.length; i++){
    dryVermouthPlaceholder[i].addEventListener('click', function(evt){
      if(hold=="dryVermouth"){
        hold = null;
        let glass = document.createElement("a-entity");
        glass.setAttribute("class", "js--pickup12 clickable");
        glass.setAttribute("gltf-model", "#bottle-glb");
        glass.setAttribute("scale", "0.15 0.15 0.15");
        glass.setAttribute("rotation", "0 90 0");
        glass.setAttribute("position", {x: this.getAttribute("position").x, y: 1.6, z: this.getAttribute("position").z});
        scene.appendChild(glass);
        glass.innerHTML += '<a-text position="-0.6 0 0" align="center" scale="1.3 1.3 1.3" rotation="0 -90 0" value="Dry Vermouth">';
        addListenersDryVermouth();
        document.getElementById("js--hold").remove();
      }
    });
  }

  for(let i = 0; i < tequillaPlaceholder.length; i++){
    tequillaPlaceholder[i].addEventListener('click', function(evt){
      if(hold=="tequilla"){
        hold = null;
        let glass = document.createElement("a-entity");
        glass.setAttribute("class", "js--pickup13 clickable");
        glass.setAttribute("gltf-model", "#bottle-glb");
        glass.setAttribute("rotation", "0 90 0");
        glass.setAttribute("scale", "0.15 0.15 0.15");
        glass.setAttribute("position", {x: this.getAttribute("position").x, y: 1.6, z: this.getAttribute("position").z});
        scene.appendChild(glass);
        glass.innerHTML += '<a-text position="-0.6 0 0" align="center" scale="1.3 1.3 1.3" rotation="0 -90 0" value="Tequilla">';
        addListenersTequilla();
        document.getElementById("js--hold").remove();
      }
    });
  }

  for(let i = 0; i < tripleSecPlaceholder.length; i++){
    tripleSecPlaceholder[i].addEventListener('click', function(evt){
      if(hold=="tripleSec"){
        hold = null;
        let glass = document.createElement("a-entity");
        glass.setAttribute("class", "js--pickup14 clickable");
        glass.setAttribute("gltf-model", "#bottle-glb");
        glass.setAttribute("scale", "0.15 0.15 0.15");
        glass.setAttribute("rotation", "0 90 0");
        glass.setAttribute("position", {x: this.getAttribute("position").x, y: 2.35, z: this.getAttribute("position").z});
        scene.appendChild(glass);
        glass.innerHTML += '<a-text position="-0.6 0 0" align="center" scale="1.3 1.3 1.3" rotation="0 -90 0" value="Triple Sec">';
        addListenersTripleSec();
        document.getElementById("js--hold").remove();
      }
    });
  }

  for(let i = 0; i < limeJuicePlaceholder.length; i++){
    limeJuicePlaceholder[i].addEventListener('click', function(evt){
      if(hold=="limeJuice"){
        hold = null;
        let glass = document.createElement("a-entity");
        glass.setAttribute("class", "js--pickup15 clickable");
        glass.setAttribute("gltf-model", "#bottle-glb");
        glass.setAttribute("scale", "0.15 0.15 0.15");
        glass.setAttribute("rotation", "0 90 0");
        glass.setAttribute("position", {x: this.getAttribute("position").x, y: 2.35, z: this.getAttribute("position").z});
        scene.appendChild(glass);
        glass.innerHTML += '<a-text position="-0.6 0 0" align="center" scale="1.3 1.3 1.3" rotation="0 -90 0" value="Lime Juice">';
        addListenersLimeJuice();
        document.getElementById("js--hold").remove();
      }
    });
  }

  for(let i = 0; i < lightRumPlaceholder.length; i++){
    lightRumPlaceholder[i].addEventListener('click', function(evt){
      if(hold=="lightRum"){
        hold = null;
        let glass = document.createElement("a-entity");
        glass.setAttribute("class", "js--pickup16 clickable");
        glass.setAttribute("gltf-model", "#bottle-glb");
        glass.setAttribute("scale", "0.15 0.15 0.15");
        glass.setAttribute("rotation", "0 90 0");
        glass.setAttribute("position", {x: this.getAttribute("position").x, y: 2.35, z: this.getAttribute("position").z});
        scene.appendChild(glass);
        glass.innerHTML += '<a-text position="-0.6 0 0" align="center" scale="1.3 1.3 1.3" rotation="0 -90 0" value="Light Rum">';
        addListenersLightRum();
        document.getElementById("js--hold").remove();
      }
    });
  }

  for(let i = 0; i < sodaWaterPlaceholder.length; i++){
    sodaWaterPlaceholder[i].addEventListener('click', function(evt){
      if(hold=="sodaWater"){
        hold = null;
        let glass = document.createElement("a-entity");
        glass.setAttribute("class", "js--pickup17 clickable");
        glass.setAttribute("gltf-model", "#bottle-glb");
        glass.setAttribute("scale", "0.15 0.15 0.15");
        glass.setAttribute("rotation", "0 90 0");
        glass.setAttribute("position", {x: this.getAttribute("position").x, y: 2.35, z: this.getAttribute("position").z});
        scene.appendChild(glass);
        glass.innerHTML += '<a-text position="-0.6 0 0" align="center" scale="1.3 1.3 1.3" rotation="0 -90 0" value="Soda Water">';
        addListenersSodaWater();
        document.getElementById("js--hold").remove();
      }
    });
  }

  for(let i = 0; i < coconutMilkPlaceholder.length; i++){
    coconutMilkPlaceholder[i].addEventListener('click', function(evt){
      if(hold=="coconutMilk"){
        hold = null;
        let glass = document.createElement("a-entity");
        glass.setAttribute("class", "js--pickup18 clickable");
        glass.setAttribute("gltf-model", "#bottle-glb");
        glass.setAttribute("scale", "0.15 0.15 0.15");
        glass.setAttribute("rotation", "0 90 0");
        glass.setAttribute("position", {x: this.getAttribute("position").x, y: 2.35, z: this.getAttribute("position").z});
        scene.appendChild(glass);
        glass.innerHTML += '<a-text position="-0.6 0 0" align="center" scale="1.3 1.3 1.3" rotation="0 -90 0" value="Coconut Milk">';
        addListenersCoconutMilk();
        document.getElementById("js--hold").remove();
      }
    });
  }

  for(let i = 0; i < vodkaPlaceholder.length; i++){
    vodkaPlaceholder[i].addEventListener('click', function(evt){
      if(hold=="vodka"){
        hold = null;
        let glass = document.createElement("a-entity");
        glass.setAttribute("class", "js--pickup24 clickable");
        glass.setAttribute("gltf-model", "#bottle-glb");
        glass.setAttribute("scale", "0.15 0.15 0.15");
        glass.setAttribute("rotation", "0 90 0");
        glass.setAttribute("position", {x: this.getAttribute("position").x, y: 2.35, z: this.getAttribute("position").z});
        scene.appendChild(glass);
        glass.innerHTML += '<a-text position="-0.6 0 0" align="center" scale="1.3 1.3 1.3" rotation="0 -90 0" value="Vodka">';
        addListenersVodka();
        document.getElementById("js--hold").remove();
      }
    });
  }
//put down mixset
  for(let i = 0; i < jiggerPlaceholder.length; i++){
    jiggerPlaceholder[i].addEventListener('click', function(evt){
      if(hold=="jigger"){
        hold = null;
        let glass = document.createElement("a-entity");
        glass.setAttribute("class", "js--pickup5 clickable js--reset");
        glass.setAttribute("gltf-model", "#jigger-glb");
        glass.setAttribute("scale", "0.08 0.08 0.08");
        glass.setAttribute("position", {x: this.getAttribute("position").x, y: 1.3, z: this.getAttribute("position").z});
        scene.appendChild(glass);
        addListenersJigger();
        document.getElementById("js--hold").remove();
        if(tutorial){
          tutorialPlane.setAttribute("visible","false");
          tutorialPlaneTextJigger.setAttribute("visible", "false");
          checkTutorial();
        }
      }
    });
  }

  for(let i = 0; i < mixerPlaceholder.length; i++){
    mixerPlaceholder[i].addEventListener('click', function(evt){
      if(hold=="mixer"){
        hold = null;
        let glass = document.createElement("a-entity");
        glass.setAttribute("class", "js--pickup3 clickable js--reset");
        glass.setAttribute("gltf-model", "#mixer-glb");
        glass.setAttribute("scale", "0.08 0.08 0.08");
        glass.setAttribute("position", {x: this.getAttribute("position").x, y: 1.33, z: this.getAttribute("position").z});
        scene.appendChild(glass);
        addListenersMixer();
        document.getElementById("js--hold").remove();
        if(tutorial){
          tutorialPlane.setAttribute("visible", "false");
          tutorialPlaneTextMixer.setAttribute("visible", "false")
          checkTutorial();
        }
      }
    });
  }

  for(let i = 0; i < lepelPlaceholder.length; i++){
    lepelPlaceholder[i].addEventListener('click', function(evt){
      if(hold=="lepel"){
        hold = null;
        let glass = document.createElement("a-entity");
        glass.setAttribute("class", "js--pickup4 clickable js--reset");
        glass.setAttribute("gltf-model", "#lepel-glb");
        glass.setAttribute("scale", "0.08 0.08 0.08");
        glass.setAttribute("position", {x: this.getAttribute("position").x, y: 1.33, z: this.getAttribute("position").z});
        scene.appendChild(glass);
        addListenersLepel();
        document.getElementById("js--hold").remove();
        if(tutorial){
          tutorialPlane.setAttribute("visible", "false");
          tutorialPlaneTextLepel.setAttribute("visible", "false")
          checkTutorial();
        }
      }
    });
  }


//ingredienten in glas
  function addGlasslisteners(){
    let glassOnPlate = document.getElementsByClassName("js--glassOnPlate");
    for(let i = 0; i < glassOnPlate.length; i++){
      glassOnPlate[i].addEventListener('click', function(evt){
        if(hold != null && hold != "jigger" && hold != "mixer" && hold != "lepel"){
          let ingredient = document.getElementById("js--hold").getAttribute("value");
          let count = 0;
          switch(ingredient){
            case "gin":
              ginAdded = true;
              pour.play();
              if(count==0){
                glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>';
                count = 1;}
              break;
            case "tonic":
              tonicAdded = true;
              pour.play();
              if(count==0){
                glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>';
                count = 1;}
              break;
            case "lemon":
              lemonAdded = true;
              drop.play();
              break;
            case "lime":
              limeAdded = true;
              drop.play();
              break;
              case "olive":
                oliveAdded = true;
                drop.play();
                break;
              case "salt":
                saltAdded = true;
                drop.play();
                break;
              case "sugar":
                sugarAdded = true;
                drop.play();
                break;
              case "mint":
                mintAdded = true;
                drop.play();
                break;
                case "pineapple":
                  pineappleAdded = true;
                  drop.play();
                  break;
                case "peachSchnapps":
                  pour.play();
                  peachSchnappsAdded = true;
                  if(count==0){
                    glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                    count = 1;}
                  break;
                case "cranberryJuice":
                  pour.play();
                  cranberryJuiceAdded = true;
                  if(count==0){
                    glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                    count = 1;}
                  break;
                case "grapeFruitJuice":
                  pour.play();
                  grapefruitJuiceAdded = true;
                  if(count==0){
                    glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                    count = 1;}
                  break;
                  case "dryVermouth":
                    pour.play();
                    dryVermouthAdded = true;
                    if(count==0){
                      glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                      count = 1;}
                    break;
                  case "tequilla":
                    pour.play();
                    tequilaAdded = true;
                    if(count==0){
                      glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                      count = 1;}
                    break;
                  case "tripleSec":
                    pour.play();
                    tripleSecAdded = true;
                    if(count==0){
                      glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                      count = 1;}
                    break;
                  case "limeJuice":
                    pour.play();
                    limeJuiceAdded = true;
                    if(count==0){
                      glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                      count = 1;}
                    break;
                    case "lightRum":
                      pour.play();
                      lightRumAdded = true;
                      if(count==0){
                        glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                        count = 1;}
                      break;
                    case "sodaWater":
                      pour.play();
                      sodaWaterAdded = true;
                      if(count==0){
                        glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                        count = 1;}
                      break;
                    case "coconutMilk":
                      pour.play();
                      coconutMilkAdded = true;
                      if(count==0){
                        glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                        count = 1;}
                      break;
                      case "vodka":
                        pour.play();
                        vodkaAdded = true;
                        if(count==0){
                          glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                          count = 1;}
                        break;
          }
        }else if(hold == "jigger"){
          let count = 0;
          jiggerUsed = true;
          let ingredient2 = jiggerContents;
          jiggerContents = "";
          switch(ingredient2){
            case "gin":
              ginAdded = true;
              pour.play();
              if(count==0){
                glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>';
                count = 1;}
              break;
            case "tonic":
              tonicAdded = true;
              pour.play();
              if(count==0){
                glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>';
                count = 1;}
              break;
            case "lemon":
              lemonAdded = true;
              drop.play();
              break;
            case "lime":
              limeAdded = true;
              drop.play();
              break;
              case "olive":
                oliveAdded = true;
                drop.play();
                break;
              case "salt":
                saltAdded = true;
                drop.play();
                break;
              case "sugar":
                sugarAdded = true;
                drop.play();
                break;
              case "mint":
                mintAdded = true;
                drop.play();
                break;
                case "pineapple":
                  pineappleAdded = true;
                  drop.play();
                  break;
                case "peachSchnapps":
                  pour.play();
                  peachSchnappsAdded = true;
                  if(count==0){
                    glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                    count = 1;}
                  break;
                case "cranberryJuice":
                  pour.play();
                  cranberryJuiceAdded = true;
                  if(count==0){
                    glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                    count = 1;}
                  break;
                case "grapeFruitJuice":
                  pour.play();
                  grapefruitJuiceAdded = true;
                  if(count==0){
                    glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                    count = 1;}
                  break;
                  case "dryVermouth":
                    pour.play();
                    dryVermouthAdded = true;
                    if(count==0){
                      glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                      count = 1;}
                    break;
                  case "tequilla":
                    pour.play();
                    tequilaAdded = true;
                    if(count==0){
                      glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                      count = 1;}
                    break;
                  case "tripleSec":
                    pour.play();
                    tripleSecAdded = true;
                    if(count==0){
                      glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                      count = 1;}
                    break;
                  case "limeJuice":
                    pour.play();
                    limeJuiceAdded = true;
                    if(count==0){
                      glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                      count = 1;}
                    break;
                    case "lightRum":
                      pour.play();
                      lightRumAdded = true;
                      if(count==0){
                        glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                        count = 1;}
                      break;
                    case "sodaWater":
                      pour.play();
                      sodaWaterAdded = true;
                      if(count==0){
                        glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                        count = 1;}
                      break;
                    case "coconutMilk":
                      pour.play();
                      coconutMilkAdded = true;
                      if(count==0){
                        glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                        count = 1;}
                      break;
                      case "vodka":
                        pour.play();
                        vodkaAdded = true;
                        if(count==0){
                          glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                          count = 1;}
                        break;
          }
        }else if(hold =="mixer"){
          for (let i = 0; i < mixerContents.length; i++){
            let count = 0;
            mixerUsed = true;
            let ingredient3 = mixerContents[i];
            switch(ingredient3){
              case "gin":
                ginAdded = true;
                pour.play();
                if(count==0){
                  glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>';
                  count = 1;}
                break;
              case "tonic":
                tonicAdded = true;
                pour.play();
                if(count==0){
                  glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>';
                  count = 1;}
                break;
              case "lemon":
                lemonAdded = true;
                drop.play();
                break;
              case "lime":
                limeAdded = true;
                drop.play();
                break;
                case "olive":
                  oliveAdded = true;
                  drop.play();
                  break;
                case "salt":
                  saltAdded = true;
                  drop.play();
                  break;
                case "sugar":
                  sugarAdded = true;
                  drop.play();
                  break;
                case "mint":
                  mintAdded = true;
                  drop.play();
                  break;
                  case "pineapple":
                    pineappleAdded = true;
                    drop.play();
                    break;
                  case "peachSchnapps":
                    pour.play();
                    peachSchnappsAdded = true;
                    if(count==0){
                      glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                      count = 1;}
                    break;
                  case "cranberryJuice":
                    pour.play();
                    cranberryJuiceAdded = true;
                    if(count==0){
                      glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                      count = 1;}
                    break;
                  case "grapeFruitJuice":
                    pour.play();
                    grapefruitJuiceAdded = true;
                    if(count==0){
                      glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                      count = 1;}
                    break;
                    case "dryVermouth":
                      pour.play();
                      dryVermouthAdded = true;
                      if(count==0){
                        glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                        count = 1;}
                      break;
                    case "tequilla":
                      pour.play();
                      tequilaAdded = true;
                      if(count==0){
                        glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                        count = 1;}
                      break;
                    case "tripleSec":
                      pour.play();
                      tripleSecAdded = true;
                      if(count==0){
                        glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                        count = 1;}
                      break;
                    case "limeJuice":
                      pour.play();
                      limeJuiceAdded = true;
                      if(count==0){
                        glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                        count = 1;}
                      break;
                      case "lightRum":
                        pour.play();
                        lightRumAdded = true;
                        if(count==0){
                          glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                          count = 1;}
                        break;
                      case "sodaWater":
                        pour.play();
                        sodaWaterAdded = true;
                        if(count==0){
                          glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                          count = 1;}
                        break;
                      case "coconutMilk":
                        pour.play();
                        coconutMilkAdded = true;
                        if(count==0){
                          glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                          count = 1;}
                        break;
                        case "vodka":
                          pour.play();
                          vodkaAdded = true;
                          if(count==0){
                            glassOnPlate[i].innerHTML += '<a-cylinder class="js--in-glass" color="lightblue"></a-cylinder>'
                            count = 1;}
                          break;
            }
            mixerContents = [];
          }
        }else if(hold == "lepel"){
          drinkStirred = true;
          stir.play();
        }
      }
    );}
  }
//cocktail api aanroepen
  for(let i = 0; i < drinkNames.length; i++){
    let drinkString = "";

    switch(i){
      case 0:
        drinkString = "margarita";
        break;
      case 1:
        drinkString = "sex+on+the+beach";
        break;
      case 2:
        drinkString = "pina+colada";
        break;
      case 3:
        drinkString = "gin+and+tonic"
        break;
      case 4:
        drinkString = "mojito";
        break;
      case 5:
        drinkString = "martini";
        break;
    }
    drinkPictures[i].setAttribute("value", drinkString);
    fetch(BASE_URL + drinkString)
      .then( (data) =>{
        return data.json();
      })
      .then( (response) =>{
        drinkNames[i].setAttribute("value", response.drinks[0].strDrink);
        drinkPictures[i].setAttribute("src", response.drinks[0].strDrinkThumb);
      });
  }

//menu board
  function openRecipe(){
    for(let i = 0; i < drinkPictures.length; i++){
      drinkPictures[i].addEventListener('click', function(evt){
        menuBoard.innerHTML += '<a-plane class="js--backRemove" color="white" width="1.4" height="1.2" position="0 0 0.02"></a-plane>';
        menuBoard.innerHTML += '<a-plane class="js--backButton clickable js--backRemove" rotation="0 0 180" width="0.2" height="0.1" position="0.5 0.48 0.03" color="red"><a-text class="js--backRemove" position="0 0 0" align="center" color="black" rotation="0 0 0" height="1" width="1" value="back"></a-text></a-plane>';
        menuBoard.innerHTML += '<a-plane class="js--backRemove" src="" id="secondPicture" rotation="0 0 180" width="0.35" height="0.35" position="0.45 -0.35 0.03"></a-plane>';
        menuBoard.innerHTML += '<a-text class="js--backRemove" id="secondName" width="1" height="1" position="0.45 -0.1 0.03" rotation="0 0 180" value="" align="center" color="black"></a-text>';
        menuBoard.innerHTML += '<a-text class="js--backRemove" width="1" height="1" position="-0.2 -0.4 0.03" value="Instructions:" align="center" color="black" rotation="0 0 180"></a-text>';
        menuBoard.innerHTML += '<a-text class="js--backRemove" id="description" width="0.8" height="1" position="-0.2 -0.25 0.03" value="" align="center" color="black" rotation="0 0 180"></a-text>';
        menuBoard.innerHTML += '<a-text class="js--backRemove" width="1" height="1" position="-0.2 0 0.03" value="Ingredients:" align="center" color="black" rotation="0 0 180"></a-text>';
        menuBoard.innerHTML += '<a-text class="js--backRemove" id="ingredients1" width="1" height="1" position="-0.2 0.08 0.03" value="" align="center" color="black" rotation="0 0 180"></a-text>'
        menuBoard.innerHTML += '<a-text class="js--backRemove" id="ingredients2" width="1" height="1" position="-0.2 0.16 0.03" value="" align="center" color="black" rotation="0 0 180"></a-text>'
        menuBoard.innerHTML += '<a-text class="js--backRemove" id="ingredients3" width="1" height="1" position="-0.2 0.24 0.03" value="" align="center" color="black" rotation="0 0 180"></a-text>'
        menuBoard.innerHTML += '<a-text class="js--backRemove" id="ingredients4" width="1" height="1" position="-0.2 0.32 0.03" value="" align="center" color="black" rotation="0 0 180"></a-text>'
        menuBoard.innerHTML += '<a-text class="js--backRemove" id="ingredients5" width="1" height="1" position="-0.2 0.40 0.03" value="" align="center" color="black" rotation="0 0 180"></a-text>'
        menuBoard.innerHTML += '<a-text class="js--backRemove" id="ingredients6" width="1" height="1" position="-0.2 0.48 0.03" value="" align="center" color="black" rotation="0 0 180"></a-text>'
        fetch(BASE_URL + drinkPictures[i].getAttribute("value"))
          .then( (data) =>{
            return data.json();
          })
          .then( (response) =>{
            let ingredients1 = document.getElementById("ingredients1");
            let ingredients2 = document.getElementById("ingredients2");
            let ingredients3 = document.getElementById("ingredients3");
            let ingredients4 = document.getElementById("ingredients4");
            let ingredients5 = document.getElementById("ingredients5");
            let ingredients6 = document.getElementById("ingredients6");
            let backButton = document.getElementsByClassName("js--backButton");
            let secondName = document.getElementById("secondName");
            let secondPicture = document.getElementById("secondPicture");
            let description = document.getElementById("description");
            secondName.setAttribute("value", response.drinks[0].strDrink);
            secondPicture.setAttribute("src", response.drinks[0].strDrinkThumb);
            description.setAttribute("value", response.drinks[0].strInstructions);

            if(response.drinks[0].strIngredient1 != null && response.drinks[0].strMeasure1 != null){
              ingredients1.setAttribute("value", response.drinks[0].strIngredient1 + " " + response.drinks[0].strMeasure1);
            }else if(response.drinks[0].strIngredient1 != null){
              ingredients1.setAttribute("value", response.drinks[0].strIngredient1);
              }

            if(response.drinks[0].strIngredient2 != null && response.drinks[0].strMeasure2 != null){
              ingredients2.setAttribute("value", response.drinks[0].strIngredient2 + " " + response.drinks[0].strMeasure2);
            }else if(response.drinks[0].strIngredient2 != null){
              ingredients2.setAttribute("value", response.drinks[0].strIngredient2);
              }

            if(response.drinks[0].strIngredient3 != null && response.drinks[0].strMeasure3 != null){
              ingredients3.setAttribute("value", response.drinks[0].strIngredient3 + " " + response.drinks[0].strMeasure3);
            }else if(response.drinks[0].strIngredient3 != null){
              ingredients3.setAttribute("value", response.drinks[0].strIngredient3);
              }

            if(response.drinks[0].strIngredient4 != null && response.drinks[0].strMeasure4 != null){
              ingredients4.setAttribute("value", response.drinks[0].strIngredient4 + " " + response.drinks[0].strMeasure4);
            }else if(response.drinks[0].strIngredient4 != null){
              ingredients4.setAttribute("value", response.drinks[0].strIngredient4);
              }

            if(response.drinks[0].strIngredient5 != null && response.drinks[0].strMeasure5 != null){
              ingredients5.setAttribute("value", response.drinks[0].strIngredient5 + " " + response.drinks[0].strMeasure5);
            }else if(response.drinks[0].strIngredient5 != null){
              ingredients5.setAttribute("value", response.drinks[0].strIngredient5);
              }

            if(response.drinks[0].strIngredient6 != null && response.drinks[0].strMeasure6 != null){
              ingredients6.setAttribute("value", response.drinks[0].strIngredient6 + " " + response.drinks[0].strMeasure6);
            }else if(response.drinks[0].strIngredient6 != null){
              ingredients6.setAttribute("value", response.drinks[0].strIngredient6);
              }

            for(let i = 0; i < backButton.length; i++){
              backButton[i].addEventListener('click', function(evt){
                removeElementsByClass("js--backRemove");
              });
            }
          });
        }
      );
    }
  }

//remove elements menu
  function removeElementsByClass(className){
    var elements = document.getElementsByClassName(className);
    while(elements.length > 0){
        elements[0].parentNode.removeChild(elements[0]);
    }
    openRecipe();
  }
//remove html class
  function removeElementsByClassName(className){
    var elements = document.getElementsByClassName(className);
    while(elements.length > 0){
        elements[0].parentNode.removeChild(elements[0]);
    }
  }
//serve button
  for(let i = 0; i < serveButton.length; i++){
    serveButton[i].addEventListener('click', function(evt){
      serveDrink(drinkToBeMade);
      removeFromGlass();
    });
  }
//skip tutorial button
  for(let i = 0; i < skipButton.length; i++){
    skipButton[i].addEventListener('click', function(evt){
      tutorial_Mixer = true;
      tutorial_Jigger = true;
      tutorial_Lepel = true;
      tutorialPlane.setAttribute("visible", "false");
      checkTutorial();
    });
  }

  for(let i = 0; i < resetButton.length; i++){
    resetButton[i].addEventListener('click', function(evt){
      removeFromGlass();
    });
  }
//set all values to false to reset
  function removeFromGlass(){
    ginAdded = false;
    tonicAdded = false;
    lemonAdded = false;
    vodkaAdded = false;
    limeAdded = false;
    peachSchnappsAdded = false;
    cranberryJuiceAdded = false;
    grapefruitJuiceAdded = false;
    dryVermouthAdded = false;
    oliveAdded = false;
    tequilaAdded = false;
    tripleSecAdded = false;
    limeJuiceAdded = false;
    saltAdded = false;
    lightRumAdded = false;
    sugarAdded = false;
    mintAdded = false;
    sodaWaterAdded = false;
    coconutMilkAdded = false;
    pineappleAdded = false;
    drinkStirred = false;
    mixerUsed = false;
    jiggerUsed = false;
    jiggerContents = "";
    mixerContents = [];
    removeElementsByClassName("js--in-glass");
  }

//check if drink was correct
  function serveDrink(objective){
    let endStr = "";
    let endStr2 = "";

    if(objective == "ginandtonic"){
      if(!ginAdded || !tonicAdded || !limeAdded){
        endStr = endStr + "Missing Ingredients: ";
        if(!ginAdded){
          endStr = endStr + "Gin ";
        }
        if(!tonicAdded){
          endStr = endStr + "Tonic ";
        }
        if(!limeAdded){
          endStr = endStr + "Lime ";
        }
      }
      if(lemonAdded || vodkaAdded || peachSchnappsAdded || cranberryJuiceAdded || grapefruitJuiceAdded || dryVermouthAdded || oliveAdded || tequilaAdded || tripleSecAdded || lightRumAdded || saltAdded || limeJuiceAdded || sugarAdded || mintAdded || sodaWaterAdded || coconutMilkAdded || pineappleAdded){
        endStr2 = endStr2 + "Wrong Ingredients: "
        if(lemonAdded){
          endStr2 = endStr2 + "Lemon ";
        }
        if(vodkaAdded){
          endStr2 = endStr2 + "Vodka ";
        }
        if(peachSchnappsAdded){
          endStr2 = endStr2 + "PeachSchnapps ";
        }
        if(cranberryJuiceAdded){
          endStr2 = endStr2 + "CranberryJuice ";
        }
        if(grapefruitJuiceAdded){
          endStr2 = endStr2 + "GrapefruitJuice ";
        }
        if(dryVermouthAdded){
          endStr2 = endStr2 + "DryVermouth ";
        }
        if(oliveAdded){
          endStr2 = endStr2 + "Olive ";
        }
        if(tequilaAdded){
          endStr2 = endStr2 + "Tequilla ";
        }
        if(tripleSecAdded){
          endStr2 = endStr2 + "TripleSec ";
        }
        if(lightRumAdded){
          endStr2 = endStr2 + "LightRum ";
        }
        if(saltAdded){
          endStr2 = endStr2 + "Salt ";
        }
        if(limeJuiceAdded){
          endStr2 = endStr2 + "LimeJuice ";
        }
        if(sugarAdded){
          endStr2 = endStr2 + "Sugar ";
        }
        if(mintAdded){
          endStr2 = endStr2 + "Mint ";
        }
        if(sodaWaterAdded){
          endStr2 = endStr2 + "SodaWater ";
        }
        if(coconutMilkAdded){
          endStr2 = endStr2 + "CoconutMilk ";
        }
        if(pineappleAdded){
          endStr2 = endStr2 + "Pineapple ";
        }
      }
      if(endStr == "" && endStr2 ==""){
        endStr = "Je hebt het drankje juist bereidt!"
      }
    }

    if(objective == "martini"){
      if(!ginAdded || !dryVermouthAdded || !oliveAdded){
        endStr = endStr + "Missing Ingredients: ";
        if(!ginAdded){
          endStr = endStr + "Gin ";
        }
        if(!dryVermouthAdded){
          endStr = endStr + "DryVermouth ";
        }
        if(!oliveAdded){
          endStr = endStr + "Olive ";
        }
      }
      if(lemonAdded || vodkaAdded || peachSchnappsAdded || cranberryJuiceAdded || grapefruitJuiceAdded || tonicAdded || limeAdded || tequilaAdded || tripleSecAdded || lightRumAdded || saltAdded || limeJuiceAdded || sugarAdded || mintAdded || sodaWaterAdded || coconutMilkAdded || pineappleAdded){
        endStr2 = endStr2 + "Wrong Ingredients: "
        if(lemonAdded){
          endStr2 = endStr2 + "Lemon ";
        }
        if(vodkaAdded){
          endStr2 = endStr2 + "Vodka ";
        }
        if(peachSchnappsAdded){
          endStr2 = endStr2 + "PeachSchnapps ";
        }
        if(cranberryJuiceAdded){
          endStr2 = endStr2 + "CranberryJuice ";
        }
        if(grapefruitJuiceAdded){
          endStr2 = endStr2 + "GrapefruitJuice ";
        }
        if(tonicAdded){
          endStr2 = endStr2 + "Tonic ";
        }
        if(limeAdded){
          endStr2 = endStr2 + "Lime ";
        }
        if(tequilaAdded){
          endStr2 = endStr2 + "Tequilla ";
        }
        if(tripleSecAdded){
          endStr2 = endStr2 + "TripleSec ";
        }
        if(lightRumAdded){
          endStr2 = endStr2 + "LightRum ";
        }
        if(saltAdded){
          endStr2 = endStr2 + "Salt ";
        }
        if(limeJuiceAdded){
          endStr2 = endStr2 + "LimeJuice ";
        }
        if(sugarAdded){
          endStr2 = endStr2 + "Sugar ";
        }
        if(mintAdded){
          endStr2 = endStr2 + "Mint ";
        }
        if(sodaWaterAdded){
          endStr2 = endStr2 + "SodaWater ";
        }
        if(coconutMilkAdded){
          endStr2 = endStr2 + "CoconutMilk ";
        }
        if(pineappleAdded){
          endStr2 = endStr2 + "Pineapple ";
        }
      }
      if(endStr == "" && endStr2 ==""){
        endStr = "Je hebt het drankje juist bereidt!"
      }
    }

    if(objective == "sexonthebeach"){
      if(!vodkaAdded || !peachSchnappsAdded || !grapefruitJuiceAdded || !cranberryJuiceAdded){
        endStr = endStr + "Missing Ingredients: ";
        if(!vodkaAdded){
          endStr = endStr + "Vodka ";
        }
        if(!peachSchnappsAdded){
          endStr = endStr + "PeachSchnapps ";
        }
        if(!grapefruitJuiceAdded){
          endStr = endStr + "GrapefruitJuice ";
        }
        if(!cranberryJuiceAdded){
          endStr = endStr + "CranberryJuice ";
        }
      }
      if(lemonAdded || ginAdded || tonicAdded || limeAdded || dryVermouthAdded || oliveAdded || tequilaAdded || tripleSecAdded || lightRumAdded || saltAdded || limeJuiceAdded || sugarAdded || mintAdded || sodaWaterAdded || coconutMilkAdded || pineappleAdded){
        endStr2 = endStr2 + "Wrong Ingredients: "
        if(lemonAdded){
          endStr2 = endStr2 + "Lemon ";
        }
        if(ginAdded){
          endStr2 = endStr2 + "Gin ";
        }
        if(tonicAdded){
          endStr2 = endStr2 + "Tonic ";
        }
        if(limeAdded){
          endStr2 = endStr2 + "Lime ";
        }
        if(dryVermouthAdded){
          endStr2 = endStr2 + "DryVermouth ";
        }
        if(oliveAdded){
          endStr2 = endStr2 + "Olive ";
        }
        if(tequilaAdded){
          endStr2 = endStr2 + "Tequilla ";
        }
        if(tripleSecAdded){
          endStr2 = endStr2 + "TripleSec ";
        }
        if(lightRumAdded){
          endStr2 = endStr2 + "LightRum ";
        }
        if(saltAdded){
          endStr2 = endStr2 + "Salt ";
        }
        if(limeJuiceAdded){
          endStr2 = endStr2 + "LimeJuice ";
        }
        if(sugarAdded){
          endStr2 = endStr2 + "Sugar ";
        }
        if(mintAdded){
          endStr2 = endStr2 + "Mint ";
        }
        if(sodaWaterAdded){
          endStr2 = endStr2 + "SodaWater ";
        }
        if(coconutMilkAdded){
          endStr2 = endStr2 + "CoconutMilk ";
        }
        if(pineappleAdded){
          endStr2 = endStr2 + "Pineapple ";
        }
      }
      if(endStr == "" && endStr2 ==""){
        endStr = "Je hebt het drankje juist bereidt!"
      }
    }

    if(objective == "margarita"){
      if(!tequilaAdded || !limeJuiceAdded || !tripleSecAdded || !saltAdded){
        endStr = endStr + "Missing Ingredients: ";
        if(!tequilaAdded){
          endStr = endStr + "Tequilla ";
        }
        if(!limeJuiceAdded){
          endStr = endStr + "LimeJuice ";
        }
        if(!tripleSecAdded){
          endStr = endStr + "TripleSec ";
        }
        if(!saltAdded){
          endStr = endStr + "Salt ";
        }
      }
      if(lemonAdded || ginAdded || tonicAdded || limeAdded || dryVermouthAdded || oliveAdded || peachSchnappsAdded || vodkaAdded || lightRumAdded || grapefruitJuiceAdded || cranberryJuiceAdded || sugarAdded || mintAdded || sodaWaterAdded || coconutMilkAdded || pineappleAdded){
        endStr2 = endStr2 + "Wrong Ingredients: "
        if(lemonAdded){
          endStr2 = endStr2 + "Lemon ";
        }
        if(ginAdded){
          endStr2 = endStr2 + "Gin ";
        }
        if(tonicAdded){
          endStr2 = endStr2 + "Tonic ";
        }
        if(limeAdded){
          endStr2 = endStr2 + "Lime ";
        }
        if(dryVermouthAdded){
          endStr2 = endStr2 + "DryVermouth ";
        }
        if(oliveAdded){
          endStr2 = endStr2 + "Olive ";
        }
        if(peachSchnappsAdded){
          endStr2 = endStr2 + "PeachSchnapps ";
        }
        if(vodkaAdded){
          endStr2 = endStr2 + "Vodka ";
        }
        if(lightRumAdded){
          endStr2 = endStr2 + "LightRum ";
        }
        if(grapefruitJuiceAdded){
          endStr2 = endStr2 + "GrapefruitJuice ";
        }
        if(cranberryJuiceAdded){
          endStr2 = endStr2 + "CranberryJuice ";
        }
        if(sugarAdded){
          endStr2 = endStr2 + "Sugar ";
        }
        if(mintAdded){
          endStr2 = endStr2 + "Mint ";
        }
        if(sodaWaterAdded){
          endStr2 = endStr2 + "SodaWater ";
        }
        if(coconutMilkAdded){
          endStr2 = endStr2 + "CoconutMilk ";
        }
        if(pineappleAdded){
          endStr2 = endStr2 + "Pineapple ";
        }
      }
      if(endStr == "" && endStr2 ==""){
        endStr = "Je hebt het drankje juist bereidt!"
    }
  }

    if(objective == "mojito"){
      if(!lightRumAdded || !limeJuiceAdded || !sugarAdded || !mintAdded || !sodaWaterAdded){
        endStr = endStr + "Missing Ingredients: ";
        if(!lightRumAdded){
          endStr = endStr + "LightRum ";
        }
        if(!limeJuiceAdded){
          endStr = endStr + "LimeJuice ";
        }
        if(!sugarAdded){
          endStr = endStr + "Sugar ";
        }
        if(!mintAdded){
          endStr = endStr + "Mint ";
        }
        if(!sodaWaterAdded){
          endStr = endStr + "SodaWater ";
        }
      }
      if(lemonAdded || ginAdded || tonicAdded || limeAdded || dryVermouthAdded || oliveAdded || tequilaAdded || tripleSecAdded || vodkaAdded || saltAdded || peachSchnappsAdded || grapefruitJuiceAdded || cranberryJuiceAdded || coconutMilkAdded || pineappleAdded){
        endStr2 = endStr2 + "Wrong Ingredients: "
        if(lemonAdded){
          endStr2 = endStr2 + "Lemon ";
        }
        if(ginAdded){
          endStr2 = endStr2 + "Gin ";
        }
        if(tonicAdded){
          endStr2 = endStr2 + "Tonic ";
        }
        if(limeAdded){
          endStr2 = endStr2 + "Lime ";
        }
        if(dryVermouthAdded){
          endStr2 = endStr2 + "DryVermouth ";
        }
        if(oliveAdded){
          endStr2 = endStr2 + "Olive ";
        }
        if(tequilaAdded){
          endStr2 = endStr2 + "Tequilla ";
        }
        if(tripleSecAdded){
          endStr2 = endStr2 + "TripleSec ";
        }
        if(vodkaAdded){
          endStr2 = endStr2 + "Vodka ";
        }
        if(saltAdded){
          endStr2 = endStr2 + "Salt ";
        }
        if(peachSchnappsAdded){
          endStr2 = endStr2 + "PeachSchnapps ";
        }
        if(grapefruitJuiceAdded){
          endStr2 = endStr2 + "GrapefruitJuice ";
        }
        if(cranberryJuiceAdded){
          endStr2 = endStr2 + "CranberryJuice ";
        }
        if(coconutMilkAdded){
          endStr2 = endStr2 + "CoconutMilk ";
        }
        if(pineappleAdded){
          endStr2 = endStr2 + "Pineapple ";
        }
      }
      if(endStr == "" && endStr2 ==""){
        endStr = "Je hebt het drankje juist bereidt!"
      }
    }

    if(objective == "pinacolada"){
      if(!lightRumAdded || !coconutMilkAdded || !pineappleAdded){
        endStr = endStr + "Missing Ingredients: ";
        if(!lightRumAdded){
          endStr = endStr + "LightRum ";
        }
        if(!coconutMilkAdded){
          endStr = endStr + "CoconutMilk ";
        }
        if(!pineappleAdded){
          endStr = endStr + "Pineapple ";
        }
      }
      if(lemonAdded || vodkaAdded || peachSchnappsAdded || cranberryJuiceAdded || grapefruitJuiceAdded || dryVermouthAdded || oliveAdded || tequilaAdded || tripleSecAdded || ginAdded || saltAdded || limeJuiceAdded || sugarAdded || mintAdded || sodaWaterAdded || tonicAdded || limeAdded){
        endStr2 = endStr2 + "Wrong Ingredients: "
        if(lemonAdded){
          endStr2 = endStr2 + "Lemon ";
        }
        if(vodkaAdded){
          endStr2 = endStr2 + "Vodka ";
        }
        if(peachSchnappsAdded){
          endStr2 = endStr2 + "PeachSchnapps ";
        }
        if(cranberryJuiceAdded){
          endStr2 = endStr2 + "CranberryJuice ";
        }
        if(grapefruitJuiceAdded){
          endStr2 = endStr2 + "GrapefruitJuice ";
        }
        if(dryVermouthAdded){
          endStr2 = endStr2 + "DryVermouth ";
        }
        if(oliveAdded){
          endStr2 = endStr2 + "Olive ";
        }
        if(tequilaAdded){
          endStr2 = endStr2 + "Tequilla ";
        }
        if(tripleSecAdded){
          endStr2 = endStr2 + "TripleSec ";
        }
        if(ginAdded){
          endStr2 = endStr2 + "Gin ";
        }
        if(saltAdded){
          endStr2 = endStr2 + "Salt ";
        }
        if(limeJuiceAdded){
          endStr2 = endStr2 + "LimeJuice ";
        }
        if(sugarAdded){
          endStr2 = endStr2 + "Sugar ";
        }
        if(mintAdded){
          endStr2 = endStr2 + "Mint ";
        }
        if(sodaWaterAdded){
          endStr2 = endStr2 + "SodaWater ";
        }
        if(tonicAdded){
          endStr2 = endStr2 + "Tonic ";
        }
        if(limeAdded){
          endStr2 = endStr2 + "Lime ";
        }
      }
      if(endStr == "" && endStr2 ==""){
        if(drinkCount == 5){endStr = "je hebt alle drankjes juist bereidt je bent een echte bartender"}else{
        endStr = "Je hebt het drankje juist bereidt!"}
      }
    }


    if(endStr == "Je hebt het drankje juist bereidt!"){

      let faultCounter = 0;
      let faultString = "";
      if(!jiggerUsed){
        faultString = faultString + "jigger";
        faultCounter = faultCounter + 1;
      }

      if(!mixerUsed){
        if(objective == "margarita" || objective == "pinacoloda"){
          if(faultCounter == 1){
            faultString = faultString + ", mixer";
            faultCounter = faultCounter + 1;
          }else{
            faultString = faultString + "mixer"
            faultCounter = faultCounter + 1;
          }
        }
      }

      if(!drinkStirred){
        if(objective == "ginandtonic" || objective == "martini"){
          if(faultCounter == 1 || faultCounter == 2){
            faultString = faultString + " en de lepel";
            faultCounter = 5;
          }else{
            faultString = faultString + "lepel"
            faultCounter = 5;
        }
      }}

      if(faultCounter == 2){
        faultString = "jigger en de mixer";
      }

      if(!faultString == ""){
        let glass = document.createElement("a-text");
        glass.setAttribute("class", "js--end-text3");
        glass.setAttribute("value", "je bent wel vergeten de "+ faultString +" te gebruiken.");
        glass.setAttribute("align", "center");
        glass.setAttribute("rotation", "0 180 0");
        glass.setAttribute("position", {x: 0, y: 2.1, z: 0.5});
        scene.appendChild(glass);
      }

      succes();
    } else if(endStr != "je hebt alle drankjes juist bereidt je bent een echte bartender"){
      console.log("failure");
      failure();
    }

    removeElementsByClassName("js--objective-text");


    let glass = document.createElement("a-text");
    glass.setAttribute("class", "js--end-text");
    glass.setAttribute("value", "");
    glass.setAttribute("align", "center");
    glass.setAttribute("rotation", "0 180 0");
    glass.setAttribute("position", {x: 0, y: 2.5, z: 0.5});
    scene.appendChild(glass);

    let endText = document.getElementsByClassName("js--end-text");
    for(let i = 0; i < endText.length; i++){
      endText[i].setAttribute("value",endStr);
    }

    let glass2 = document.createElement("a-text");
    glass2.setAttribute("class", "js--end-text2");
    glass2.setAttribute("value", "");
    glass2.setAttribute("align", "center");
    glass2.setAttribute("rotation", "0 180 0");
    glass2.setAttribute("position", {x: 0, y: 2.1, z: 0.5});
    scene.appendChild(glass2);

    let endText2 = document.getElementsByClassName("js--end-text2");
    for(let i = 0; i < endText2.length; i++){
      endText2[i].setAttribute("value",endStr2);
    }
  }

  function failure(){
    let glass = document.createElement("a-plane");
    glass.setAttribute("class", "js--try-again-button clickable");
    glass.setAttribute("width", "2.1");
    glass.setAttribute("height", "0.7");
    glass.setAttribute("color", "red");
    glass.setAttribute("rotation", "0 180 0");
    glass.setAttribute("position", {x: 0, y: 1.3, z: 0.5});
    scene.appendChild(glass);
    let tryAgainButton = document.getElementsByClassName("js--try-again-button");
    for(let i = 0; i < tryAgainButton.length; i++){
      tryAgainButton[i].innerHTML += '<a-text position="0 0 0" align="center" color="black" rotation="0 0 0" height="10" width="10" value="Try Again">';
      tryAgainButton[i].addEventListener('click', function(evt){
        removeElementsByClassName("js--end-text");
        removeElementsByClassName("js--end-text2");
        removeElementsByClassName("js--end-text3");
        setDrinkToBeMade(drinkToBeMade);
        removeElementsByClassName("js--try-again-button");
      });
    }
  }

  function succes(){
    let glass = document.createElement("a-plane");
    glass.setAttribute("class", "js--try-again-button clickable");
    glass.setAttribute("width", "2.1");
    glass.setAttribute("height", "0.7");
    glass.setAttribute("color", "red");
    glass.setAttribute("rotation", "0 180 0");
    glass.setAttribute("position", {x: 0, y: 1.3, z: 0.5});
    scene.appendChild(glass);
    let tryAgainButton = document.getElementsByClassName("js--try-again-button");
    for(let i = 0; i < tryAgainButton.length; i++){
      tryAgainButton[i].innerHTML += '<a-text position="0 0 0" align="center" color="black" rotation="0 0 0" height="10" width="10" value="New Drink">';
      tryAgainButton[i].addEventListener('click', function(evt){
        removeElementsByClassName("js--end-text");
        removeElementsByClassName("js--end-text2");
        removeElementsByClassName("js--end-text3");
        setDrinkToBeMade(drinkToBeMade + drinkToBeMade);
        removeElementsByClassName("js--try-again-button");
      });
    }
  }
//select random drink to make
  function setDrinkToBeMade(drinkAgain){
    let randomNumber = Math.floor(Math.random() * 6);
    let drinkStr = null;

    if(drinkAgain == "ginandtonic"){
      randomNumber = 0;
    }
    if(drinkAgain == "martini"){
      randomNumber = 1;
    }
    if(drinkAgain == "sexonthebeach"){
      randomNumber = 2;
    }
    if(drinkAgain == "margarita"){
      randomNumber = 3;
    }
    if(drinkAgain == "mojito"){
      randomNumber = 4;
    }
    if(drinkAgain == "pinacolada"){
      randomNumber = 5;
    }
    if(drinkAgain == "ginandtonicginandtonic"){
      randomNumber = 1;
      drinkCount = drinkCount + 1;
    }
    if(drinkAgain == "martinimartini"){
      randomNumber = 2;
      drinkCount = drinkCount + 1;
    }
    if(drinkAgain == "sexonthebeachsexonthebeach"){
      randomNumber = 3;
      drinkCount = drinkCount + 1;
    }
    if(drinkAgain == "margaritamargarita"){
      randomNumber = 4;
      drinkCount = drinkCount + 1;
    }
    if(drinkAgain == "mojitomojito"){
      randomNumber = 5;
      drinkCount = drinkCount + 1;
    }
    if(drinkAgain == "pinacoladapinacolda"){
      randomNumber = 0;
      drinkCount = drinkCount + 1;
    }

    switch(randomNumber){
    case 0:
      drinkToBeMade = "ginandtonic";
      drinkStr = "Gin and Tonic";
      break;
    case 1:
      drinkToBeMade = "martini";
      drinkStr = "Martini"
      break;
    case 2:
      drinkToBeMade = "sexonthebeach";
      drinkStr = "Sex on the Beach"
      break;
    case 3:
      drinkToBeMade = "margarita";
      drinkStr = "Margarita"
      break;
    case 4:
      drinkToBeMade = "mojito";
      drinkStr = "Mojito"
      break;
    case 5:
      drinkToBeMade = "pinacolada";
      drinkStr = "Pinacolada";
      break;
    }

    let glass = document.createElement("a-text");
    glass.setAttribute("class", "js--objective-text");
    glass.setAttribute("value", "");
    glass.setAttribute("align", "center");
    glass.setAttribute("rotation", "0 180 0");
    glass.setAttribute("position", {x: 0, y: 2.5, z: 0.5});
    scene.appendChild(glass);


    let objectiveText = document.getElementsByClassName("js--objective-text");
    for(let i = 0; i < objectiveText.length; i++){
      objectiveText[i].setAttribute("value","Objective: make a " + drinkStr);
  }

}}
